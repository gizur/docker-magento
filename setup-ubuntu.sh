#!/bin/bash

# http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/configuring_nginx_for_magento

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# set mysql password (if it's not already set)
mysqladmin -u root password mysql-server
echo -e "<?php\nphpinfo();\n " > /var/www/html/info.php
DEBIAN_FRONTEND=noninteractive apt-get install -y phpmyadmin

# remove previous version
rm -f /tmp/magento.sql
rm -rf /var/www/html/magento
echo "drop database magento" | mysql -uroot -pmysql-server
echo "drop user 'magento'@'localhost'" | mysql -uroot -pmysql-server

# install magento
tar -xzf magento-1.9.1.1.tar.gz
mv magento /var/www/html/
chown www-data:www-data -R /var/www/html/
cd /var/www/html/; find . -type d -exec chmod 700 {} \;
cd /var/www/html/; find . -type f -exec chmod 600 {} \;
cd $DIR;

chmod a+r -R /var/www/html/

echo "CREATE DATABASE magento;" | mysql -uroot -pmysql-server
echo "CREATE USER magento@localhost IDENTIFIED BY 'magento';" | mysql -uroot -pmysql-server
echo "GRANT ALL PRIVILEGES ON magento.* TO magento@localhost IDENTIFIED BY 'magento';" | mysql -uroot -pmysql-server

cp magento-check.php /var/www/html/magento/

Intro
-----

Magento has a larget market share than WooCommerce. WooCommerce is a WordPress plugin.
Magento is owned by eBay. 

 * http://blog.aheadworks.com/2014/10/e-commerce-platforms-share-investigation-october-2014/
 * http://tomrobertshaw.net/2014/04/april-2014-ecommerce-survey/

Community edition is downloaded here: https://www.magentocommerce.com/download


Setup
-----

 * Admin panel: [IP]:[PORT]/magento/index.php/admin/
 * Admin credentials: admin/magento1
 * Database credentials: magento/magento (db: magento)

Update `/etc/apache2/apache2.conf` with these settings for `/var/www/` (needed to .htaccess to work):

```
Options FollowSymLinks
AllowOverride All
```

phpmyadmin
---------

Add `IncludeOptional ../phpmyadmin/apache.conf` to the end of `/etc/apache2/apache2.conf`

#!/bin/bash

# http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/configuring_nginx_for_magento

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

cp magento.sql.gz /tmp
gunzip /tmp/magento.sql.gz
mysql -umagento -pmagento magento < /tmp/magento.sql

cp ./app-etc-config.xml /var/www/html/magento/app/etc/config.xml
cp ./app-etc-local.xml /var/www/html/magento/app/etc/local.xml

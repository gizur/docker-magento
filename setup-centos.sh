#!/bin/bash

# http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/configuring_nginx_for_magento

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# remove previous version
rm -f /tmp/magento.sql
rm -rf /usr/share/nginx/html/magento
echo "drop database magento" | mysql -uroot -pmysql-server
echo "drop user 'magento'@'localhost'" | mysql -uroot -pmysql-server

# install magento
tar -xzf magento-1.9.1.1.tar.gz
mv magento /usr/share/nginx/html/
chown apache:apache -R /usr/share/nginx/html/
cd /usr/share/nginx/html/; find . -type d -exec chmod 700 {} \;
cd /usr/share/nginx/html/; find . -type f -exec chmod 600 {} \;
cd $DIR;

chmod a+r -R /usr/share/nginx/html/

echo "CREATE DATABASE magento;" | mysql -uroot -pmysql-server
echo "CREATE USER magento@localhost IDENTIFIED BY 'magento';" | mysql -uroot -pmysql-server
echo "GRANT ALL PRIVILEGES ON magento.* TO magento@localhost IDENTIFIED BY 'magento';" | mysql -uroot -pmysql-server

cp magento.sql.gz /tmp
gunzip /tmp/magento.sql.gz
mysql -umagento -pmagento magento < /tmp/magento.sql

cp ./app-etc-config.xml /usr/share/nginx/html/magento/app/etc/config.xml
cp ./app-etc-local.xml /usr/share/nginx/html/magento/app/etc/local.xml

# Have a problem with CSS that needs to be solved
#mkdir /usr/share/nginx/html/magento/media/css
#chown apache:apache /usr/share/nginx/html/magento/media/css
